Rstats: Introduction to ANOVA, linear models, and tidymodels
================
UQ Library
2021-12-17

-   [What are we going to learn?](#what-are-we-going-to-learn)
-   [Essential shortcuts](#essential-shortcuts)
-   [Material](#material)
-   [About the data](#about-the-data)
-   [Statistics in R using `{base}` and
    `{stats}`](#statistics-in-r-using-base-and-stats)
    -   [Visualize the data](#visualize-the-data)
    -   [Analysis of Variance (ANOVA)](#analysis-of-variance-anova)
    -   [Linear Model](#linear-model)
-   [Introducing Tidymodels](#introducing-tidymodels)
    -   [Build and fit a model](#build-and-fit-a-model)
-   [Use a model to predict](#use-a-model-to-predict)
-   [Close project](#close-project)
-   [Useful links](#useful-links)

## What are we going to learn?

During this session, you will:

-   analysis of variance (ANOVA) in base R
-   linear model in base R
-   the tidymodel approach

## Essential shortcuts

Remember some of the most commonly used RStudio shortcuts:

-   function or dataset help: press <kbd>F1</kbd> with your cursor
    anywhere in a function name.
-   execute from script: <kbd>Ctrl</kbd> + <kbd>Enter</kbd>
-   assignment operator (`<-`): <kbd>Alt</kbd> + <kbd>-</kbd>

## Material

**Load the following packages**:

``` r
library(tidymodels) # for parsnip package and rest of tidymodels

# helper packages
library(readr)       # for importing data
library(kableExtra)  # pretty tables
library(car)         # Companion to Applied Regression package
library(performance) # Assessment of Regression Models performance
library(dotwhisker)  # for visualizing regression results
library(parsnip)     # tidy model 'engines'
```

> Remember to use <kbd>Ctrl</kbd>+<kbd>Enter</kbd> to execute a command
> from the script.

## About the data

The following section will be using data from [Constable
(1993)](https://link.springer.com/article/10.1007/BF00349318) to explore
how three different feeding regimes affect the size of sea urchins over
time.

Sea urchins reportedly regulate their size according to the level of
food available to regulate maintenance requirements. The paper examines
whether a reduction in suture width (i.e., connection points between
plates; see Fig 1 from constable 1993) is the basis for shrinking due to
low food conditions.

![Figure 1 from Constable 1993 paper showing sea urchin plates and
suture width](images/Constable-1993-fig1.PNG)

The [data in csv
format](https://tidymodels.org/start/models/urchins.csv) is available
from the tidymodels website and were assembled for a tutorial
[here](https://www.flutterbys.com.au/stats/tut/tut7.5a.html).

``` r
urchins <- 
   # read in the data
   read_csv("https://tidymodels.org/start/models/urchins.csv") %>% 
   # change the names to be more description
setNames(c("food_regime", "initial_volume", "width")) %>% 
   # convert food_regime from chr to a factor, helpful for modeling
   mutate(food_regime = factor(food_regime, 
                               levels = c("Initial", "Low", "High")))
```

The data is a (tibble)\[<https://tibble.tidyverse.org/index.html>\].

``` r
urchins # see the data as a tibble
```

    ## # A tibble: 72 x 3
    ##    food_regime initial_volume width
    ##    <fct>                <dbl> <dbl>
    ##  1 Initial                3.5 0.01 
    ##  2 Initial                5   0.02 
    ##  3 Initial                8   0.061
    ##  4 Initial               10   0.051
    ##  5 Initial               13   0.041
    ##  6 Initial               13   0.061
    ##  7 Initial               15   0.041
    ##  8 Initial               15   0.071
    ##  9 Initial               16   0.092
    ## 10 Initial               17   0.051
    ## # ... with 62 more rows

Or a kable’d table:

``` r
urchins %>% 
   kable() %>% 
   kable_classic(full_width = FALSE) %>% 
   scroll_box(height = "500px")
```

<div
style="border: 1px solid #ddd; padding: 0px; overflow-y: scroll; height:500px; ">

<table class=" lightable-classic" style="font-family: &quot;Arial Narrow&quot;, &quot;Source Sans Pro&quot;, sans-serif; width: auto !important; margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;">
food_regime
</th>
<th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;">
initial_volume
</th>
<th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;">
width
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
3.5
</td>
<td style="text-align:right;">
0.010
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
5.0
</td>
<td style="text-align:right;">
0.020
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
8.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
10.0
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
13.0
</td>
<td style="text-align:right;">
0.041
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
13.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
15.0
</td>
<td style="text-align:right;">
0.041
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
15.0
</td>
<td style="text-align:right;">
0.071
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
16.0
</td>
<td style="text-align:right;">
0.092
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
17.0
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
19.0
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
20.0
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
21.0
</td>
<td style="text-align:right;">
0.102
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
21.0
</td>
<td style="text-align:right;">
0.092
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
24.0
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
24.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
24.0
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
28.0
</td>
<td style="text-align:right;">
0.071
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
29.0
</td>
<td style="text-align:right;">
0.071
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
35.0
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
36.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
39.0
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
39.0
</td>
<td style="text-align:right;">
0.112
</td>
</tr>
<tr>
<td style="text-align:left;">
Initial
</td>
<td style="text-align:right;">
44.0
</td>
<td style="text-align:right;">
0.102
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
5.0
</td>
<td style="text-align:right;">
0.041
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
8.0
</td>
<td style="text-align:right;">
0.031
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
8.5
</td>
<td style="text-align:right;">
0.041
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
11.5
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
10.0
</td>
<td style="text-align:right;">
0.071
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
14.0
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
15.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
15.5
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
18.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
18.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
18.0
</td>
<td style="text-align:right;">
0.071
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
22.0
</td>
<td style="text-align:right;">
0.041
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
21.5
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
20.5
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
25.0
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
26.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
38.0
</td>
<td style="text-align:right;">
0.071
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
41.0
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
41.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
43.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
45.0
</td>
<td style="text-align:right;">
0.071
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
7.5
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
9.5
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
9.0
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
9.0
</td>
<td style="text-align:right;">
0.092
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
12.0
</td>
<td style="text-align:right;">
0.092
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
13.0
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
14.5
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
15.0
</td>
<td style="text-align:right;">
0.102
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
13.0
</td>
<td style="text-align:right;">
0.112
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
11.5
</td>
<td style="text-align:right;">
0.071
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
14.0
</td>
<td style="text-align:right;">
0.071
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
14.0
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
15.5
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
15.0
</td>
<td style="text-align:right;">
0.092
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
17.5
</td>
<td style="text-align:right;">
0.102
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
19.0
</td>
<td style="text-align:right;">
0.122
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
19.0
</td>
<td style="text-align:right;">
0.102
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
19.5
</td>
<td style="text-align:right;">
0.163
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
20.0
</td>
<td style="text-align:right;">
0.051
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
23.5
</td>
<td style="text-align:right;">
0.112
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
30.0
</td>
<td style="text-align:right;">
0.153
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
26.0
</td>
<td style="text-align:right;">
0.082
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
29.0
</td>
<td style="text-align:right;">
0.122
</td>
</tr>
<tr>
<td style="text-align:left;">
High
</td>
<td style="text-align:right;">
39.0
</td>
<td style="text-align:right;">
0.102
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
47.5
</td>
<td style="text-align:right;">
0.041
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
46.5
</td>
<td style="text-align:right;">
0.061
</td>
</tr>
<tr>
<td style="text-align:left;">
Low
</td>
<td style="text-align:right;">
11.5
</td>
<td style="text-align:right;">
0.031
</td>
</tr>
</tbody>
</table>

</div>

We have 72 urchins with data on:

-   experimental feeding regime group with 3 levels (Initial, Low, or
    High)
-   size in milliliters at the start of the experiment (initial_volume)
-   suture width in millimeters at the end of the experiment (width, see
    [Fig 1](pictures/Constable-1993-fig1.PNG))

## Statistics in R using `{base}` and `{stats}`

### Visualize the data

Use a boxplot to visualize width versus `food_regime` as a factor and a
scatter plot for width versus `initial_volume` as a continuous variable.

``` r
boxplot(width ~ food_regime, data = urchins)
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-2-1.png)<!-- -->

``` r
plot(width ~ initial_volume, data = urchins)
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-2-2.png)<!-- -->

We can see that there are some relationships between the response
variable (width) and our two covariates (food_regime and initial
volume). But what about the interaction between the two covariates?

##### Challenge 1 - use ggplot2 to make a plot visualizing the interaction between our two variables. Add a trend line to the data.

> Hint: think about grouping and coloring.

``` r
ggplot(urchins,
       aes(x = initial_volume,
           y = width,
           group = food_regime,
           col = food_regime)) +
   geom_point() +
   geom_smooth(method = lm, se = FALSE) +
   scale_colour_viridis_d(option = "plasma", end = 0.7)
```

    ## `geom_smooth()` using formula 'y ~ x'

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-3-1.png)<!-- -->

Urchins that were larger in volume at the start of the experiment tended
to have wider sutures at the end. Slopes of the lines look different so
this effect may depend on the feeding regime indicating we should
include an interaction term.

### Analysis of Variance (ANOVA)

Information in this section was taken from
[rpubs.com](https://rpubs.com/tmcurley/twowayanova) and [Data Analysis
in R Ch
7](https://bookdown.org/steve_midway/DAR/understanding-anova-in-r.html#multiple-comparisons).

We can do an ANOVA with the `aov()` function to test for differences in
sea urchin suture width between our groups. We are technically running
and **analysis of covariance (ANCOVA)** as we have both a continuous and
a categorical variable. ANOVAs are for categorical variables and we will
see that some of the *post-hoc* tests are not amenable to continuous
variables.

> `aov()` uses the model formula
> `response variable ~ covariate1 + covariate2`. The \* denotes the
> inclusion of both main effects and interactions which we have done
> below. The formula below is equivalent to
> `reponse ~ covar1 + covar2 + covar1:covar2` i.e. the main effect of
> covar 1 and covar 2, and the interaction between the two.

``` r
aov_urch <- aov(width ~ food_regime * initial_volume, 
                data = urchins)
summary(aov_urch) # print the summary statistics
```

    ##                            Df   Sum Sq  Mean Sq F value   Pr(>F)    
    ## food_regime                 2 0.012380 0.006190  13.832 9.62e-06 ***
    ## initial_volume              1 0.008396 0.008396  18.762 5.15e-05 ***
    ## food_regime:initial_volume  2 0.004609 0.002304   5.149  0.00835 ** 
    ## Residuals                  66 0.029536 0.000448                     
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Both the main effects and interaction are significant (p \< 0.05)
indicating a significant interactive effect between food regime and
initial volume on urchin suture width. We need to do a
pairwise-comparison to find out which factor levels and combination of
the two covariates have the largest effect on width.

#### Pair-wise comparison

Run a **Tukey’s Honestly Significant Difference (HSD)** test

> It does not work for non-factors as per the warning message.

``` r
TukeyHSD(aov_urch)
```

    ##   Tukey multiple comparisons of means
    ##     95% family-wise confidence level
    ## 
    ## Fit: aov(formula = width ~ food_regime * initial_volume, data = urchins)
    ## 
    ## $food_regime
    ##                      diff          lwr         upr     p adj
    ## Low-Initial  -0.006791667 -0.021433881 0.007850548 0.5100502
    ## High-Initial  0.023791667  0.009149452 0.038433881 0.0006687
    ## High-Low      0.030583333  0.015941119 0.045225548 0.0000129

The comparison between *High-Initial* and *High-Low* food regimes are
significant (p \< 0.05).

#### Checking the model

We also want to check that our model is a good fit and does not violate
any ANOVA **assumptions**:

1.  Data are independent and normally distributed.
2.  The residuals from the data are normally distributed.
3.  The variances of the sampled populations are equal.

##### Challenge 2 - Use a histogram and qqplots to visually check data are normal.

``` r
hist(urchins$width)
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-6-1.png)<!-- -->

``` r
qqnorm(urchins$width)
qqline(urchins$width)
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-6-2.png)<!-- -->

You could also run a Shapiro-Wilk test on the data:

``` r
shapiro.test(urchins$width)
```

    ## 
    ##  Shapiro-Wilk normality test
    ## 
    ## data:  urchins$width
    ## W = 0.95726, p-value = 0.01552

Check the **model residuals**. Plot the residuals vs fitted values - do
not want too much deviation from 0.

``` r
plot(aov_urch, 1)
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-8-1.png)<!-- -->

``` r
plot(predict(aov_urch) ~ aov_urch$fitted.values)
abline(0, 1, col = "red")
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-8-2.png)<!-- -->

Check the **normality of residuals**, run Shapiro-Wilk test on
residuals:

``` r
plot(aov_urch, 2)
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-9-1.png)<!-- -->

``` r
shapiro.test(resid(aov_urch))
```

    ## 
    ##  Shapiro-Wilk normality test
    ## 
    ## data:  resid(aov_urch)
    ## W = 0.98456, p-value = 0.5244

The residuals fall on the Normal Q-Q plot diagonal and the Shapiro-Wilk
result is non-significant (p \> 0.05).

Check for **homogeneity of variance**

##### Challenge 3 - use the help documentation for `leveneTest()` from the `car` package to check homogenetity of variance on `food_regime`.

> Again, only works for factor groups.

``` r
leveneTest(width ~ food_regime, data = urchins)
```

    ## # A tibble: 2 x 3
    ##      Df `F value` `Pr(>F)`
    ##   <int>     <dbl>    <dbl>
    ## 1     2      4.42   0.0156
    ## 2    69     NA     NA

The Levene’s Test is significant for `food_regime` (not what we want)
and there are a few options to deal with this. You can ignore this
violation based on your own *a priori* knowledge of the distribution of
the population being samples, drop the p-value significance, or use a
different test.

### Linear Model

``` r
lm_urch <- lm(width ~ food_regime * initial_volume, 
              data = urchins)
summary(lm_urch)
```

    ## 
    ## Call:
    ## lm(formula = width ~ food_regime * initial_volume, data = urchins)
    ## 
    ## Residuals:
    ##       Min        1Q    Median        3Q       Max 
    ## -0.045133 -0.013639  0.001111  0.013226  0.067907 
    ## 
    ## Coefficients:
    ##                                  Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)                     0.0331216  0.0096186   3.443 0.001002 ** 
    ## food_regimeLow                  0.0197824  0.0129883   1.523 0.132514    
    ## food_regimeHigh                 0.0214111  0.0145318   1.473 0.145397    
    ## initial_volume                  0.0015546  0.0003978   3.908 0.000222 ***
    ## food_regimeLow:initial_volume  -0.0012594  0.0005102  -2.469 0.016164 *  
    ## food_regimeHigh:initial_volume  0.0005254  0.0007020   0.748 0.456836    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.02115 on 66 degrees of freedom
    ## Multiple R-squared:  0.4622, Adjusted R-squared:  0.4215 
    ## F-statistic: 11.35 on 5 and 66 DF,  p-value: 6.424e-08

In the output, we have the model call, residuals, and the coefficients.
The first coefficient is the `(Intercept)` and you might notice the
`food_regimeInitial` is missing. The function defaults to an effects
parameterization where the intercept is the reference or baseline of the
categorical group - Initial in this case.

> You can change the reference level of a factor using the `relevel()`
> function.

The estimates of the remaining group levels of `food_regime` represents
the effect of being in that group. To calculate the group coefficients
for all group levels you *add* the estimates for the level to the
intercept (first group level) estimate. For example, the estimate for
the *Initial* feeding regime is 0.0331 and we add the estimate of *Low*
(0.0331 + 0.0197) to get the mean maximum size of 0.0528 mm for width.

For the continuous covariate, the estimate represents the change in the
response variable for a unit increase in the covariate. ‘Initial
Volume’s’ estimate of 0.0015 represents a 0.0015 mm increase (the
estimate is positive) in width per ml increase in urchin initial volume.

We can get ANOVA test statistics on our linear model using the `anova()`
in base or `Anova()` from the `car` package.

``` r
anova(lm_urch)
```

    ## # A tibble: 4 x 5
    ##      Df `Sum Sq` `Mean Sq` `F value`    `Pr(>F)`
    ##   <int>    <dbl>     <dbl>     <dbl>       <dbl>
    ## 1     2  0.0124   0.00619      13.8   0.00000962
    ## 2     1  0.00840  0.00840      18.8   0.0000515 
    ## 3     2  0.00461  0.00230       5.15  0.00835   
    ## 4    66  0.0295   0.000448     NA    NA

``` r
Anova(lm_urch)
```

    ## # A tibble: 4 x 4
    ##   `Sum Sq`    Df `F value`     `Pr(>F)`
    ##      <dbl> <dbl>     <dbl>        <dbl>
    ## 1  0.0169      2     18.8   0.000000336
    ## 2  0.00840     1     18.8   0.0000515  
    ## 3  0.00461     2      5.15  0.00835    
    ## 4  0.0295     66     NA    NA

These are effectively the same as the `aov()` model we ran before.

> **Note**: The statistics outputs are the same comparing the `aov()`
> and `anova()` models while the `Anova()` model is **not** exactly the
> same. The `Anova()` output tells us it was a Type II test and the
> `aov()` documentation says it is only for *balanced* designs which
> means the Type 1 test is the applied (see
> [here](https://bookdown.org/ndphillips/YaRrr/type-i-type-ii-and-type-iii-anovas.html)).
> The type of test can be set for `Anova()` but not the others. Here,
> the overall take-away from the different ANOVA functions are
> comparable. It is always a good idea to have a read of the
> documentation of new functions you are using in R especially
> statistics related functions so you know what the argument defaults
> are etc.

##### Challenge 4 - use the check_model() documentation to apply the function to our `lm_urch` model.

The performance package has a handy function `check_model()` that will
check several aspects of your model in one go:

``` r
check_model(lm_urch)
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-14-1.png)<!-- -->

## Introducing Tidymodels

Like the tidyverse package, the tidymodels framework is a collection of
packages for modeling and machine learning following the tidyverse
principles.

This section is modified from the first [Tidymodels
article](https://www.tidymodels.org/start/models/).

There are many linear regression modeling packages (`{lme4}`, `{nlme}`,
`{lmerTest}`, etc.) all of which vary in the way you define the model
formula etc.

### Build and fit a model

Let’s apply a standard two-way analysis of variance (ANOVA) model to the
dataset as we did before. For this kind of model, ordinary least squares
is a good initial approach.

For Tidymodels, we need to specify the following:

1.  The *functional form* using the [parsnip
    package](https://parsnip.tidymodels.org/).
2.  The *method for fitting* the model by setting the **engine**.

We will specify the *functional form* or model type as [“linear
regression”](https://parsnip.tidymodels.org/reference/linear_reg.html)
as there is a numeric outcome with a linear slope and intercept. We can
do this with:

``` r
linear_reg()  
```

    ## Linear Regression Model Specification (regression)
    ## 
    ## Computational engine: lm

On its own, not that interesting. Next, we specify the method for
*fitting* or training the model using the `set_engine()` function. The
engine value is often a mash-up of the software that can be used to fit
or train the model as well as the estimation method. For example, to use
ordinary least squares, we can set the engine to be `lm`.

The [documentation
page](https://parsnip.tidymodels.org/reference/linear_reg.html) for
linear_reg() lists the possible engines. We’ll save this model object as
lm_mod.

``` r
lm_mod <- 
linear_reg() %>% 
   set_engine("lm")
```

Next, the model can be estimated or trained using the `fit()` function
and the model formula we used for the ANOVAs:

`width ~ initial_volume * food_regime`

``` r
lm_fit <- 
   lm_mod %>% 
   fit(width ~ initial_volume * food_regime, data = urchins)

lm_fit
```

    ## parsnip model object
    ## 
    ## Fit time:  0ms 
    ## 
    ## Call:
    ## stats::lm(formula = width ~ initial_volume * food_regime, data = data)
    ## 
    ## Coefficients:
    ##                    (Intercept)                  initial_volume  
    ##                      0.0331216                       0.0015546  
    ##                 food_regimeLow                 food_regimeHigh  
    ##                      0.0197824                       0.0214111  
    ##  initial_volume:food_regimeLow  initial_volume:food_regimeHigh  
    ##                     -0.0012594                       0.0005254

We can use the `tidy()` function for our `lm` object to output model
parameter estimates and their statistical properties and use `kable()`
to make a neat table. Similar to `summary()` but the results are more
predictable and useful format.

``` r
tidy(lm_fit) %>% 
   kable() %>% 
   kable_classic(full_width = FALSE)
```

<table class=" lightable-classic" style="font-family: &quot;Arial Narrow&quot;, &quot;Source Sans Pro&quot;, sans-serif; width: auto !important; margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
term
</th>
<th style="text-align:right;">
estimate
</th>
<th style="text-align:right;">
std.error
</th>
<th style="text-align:right;">
statistic
</th>
<th style="text-align:right;">
p.value
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
(Intercept)
</td>
<td style="text-align:right;">
0.0331216
</td>
<td style="text-align:right;">
0.0096186
</td>
<td style="text-align:right;">
3.4434873
</td>
<td style="text-align:right;">
0.0010020
</td>
</tr>
<tr>
<td style="text-align:left;">
initial_volume
</td>
<td style="text-align:right;">
0.0015546
</td>
<td style="text-align:right;">
0.0003978
</td>
<td style="text-align:right;">
3.9077643
</td>
<td style="text-align:right;">
0.0002220
</td>
</tr>
<tr>
<td style="text-align:left;">
food_regimeLow
</td>
<td style="text-align:right;">
0.0197824
</td>
<td style="text-align:right;">
0.0129883
</td>
<td style="text-align:right;">
1.5230864
</td>
<td style="text-align:right;">
0.1325145
</td>
</tr>
<tr>
<td style="text-align:left;">
food_regimeHigh
</td>
<td style="text-align:right;">
0.0214111
</td>
<td style="text-align:right;">
0.0145318
</td>
<td style="text-align:right;">
1.4733993
</td>
<td style="text-align:right;">
0.1453970
</td>
</tr>
<tr>
<td style="text-align:left;">
initial_volume:food_regimeLow
</td>
<td style="text-align:right;">
-0.0012594
</td>
<td style="text-align:right;">
0.0005102
</td>
<td style="text-align:right;">
-2.4685525
</td>
<td style="text-align:right;">
0.0161638
</td>
</tr>
<tr>
<td style="text-align:left;">
initial_volume:food_regimeHigh
</td>
<td style="text-align:right;">
0.0005254
</td>
<td style="text-align:right;">
0.0007020
</td>
<td style="text-align:right;">
0.7484702
</td>
<td style="text-align:right;">
0.4568356
</td>
</tr>
</tbody>
</table>

This output can be used to generate a dot-and-whisker plot of our
regression results using the `dotwhisker` package:

``` r
tidy(lm_fit) %>% 
   dwplot(dot_args = list(size = 2, color = "black"),
          whisker_args = list(color = "black"),
          vline = geom_vline(xintercept = 0, 
                             color = "grey50",
                             linetype = 2))
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-19-1.png)<!-- -->

## Use a model to predict

Say that it would be interesting to make a plot of the mean body size
for urchins that started the experiment with an initial volume of 20 ml.

First, lets make some new example data to predict for our graph:

``` r
new_points <- expand.grid(initial_volume = 20,
                          food_regime = c("Initial", "Low", "High"))
new_points
```

    ## # A tibble: 3 x 2
    ##   initial_volume food_regime
    ##            <dbl> <fct>      
    ## 1             20 Initial    
    ## 2             20 Low        
    ## 3             20 High

We can then use the `predict()` function to find the mean values at 20
ml initial volume.

With tidymodels, the types of predicted values are standardized so that
we can use the same syntax to get these values.

Let’s generate the mean suture width values:

``` r
mean_pred <- predict(lm_fit, new_data = new_points)
mean_pred
```

    ## # A tibble: 3 x 1
    ##    .pred
    ##    <dbl>
    ## 1 0.0642
    ## 2 0.0588
    ## 3 0.0961

When making predictions, the tidymodels convention is to always produce
a tibble of results with standardized column names. This makes it easy
to combine the original data and the predictions in a usable format:

``` r
conf_int_pred <- predict(lm_fit, 
                         new_data = new_points,
                         type = "conf_int")
conf_int_pred
```

    ## # A tibble: 3 x 2
    ##   .pred_lower .pred_upper
    ##         <dbl>       <dbl>
    ## 1      0.0555      0.0729
    ## 2      0.0499      0.0678
    ## 3      0.0870      0.105

``` r
# now combine:
plot_data <- 
   new_points %>% 
   bind_cols(mean_pred, conf_int_pred)

plot_data
```

    ## # A tibble: 3 x 5
    ##   initial_volume food_regime  .pred .pred_lower .pred_upper
    ##            <dbl> <fct>        <dbl>       <dbl>       <dbl>
    ## 1             20 Initial     0.0642      0.0555      0.0729
    ## 2             20 Low         0.0588      0.0499      0.0678
    ## 3             20 High        0.0961      0.0870      0.105

``` r
# and plot:
ggplot(plot_data, 
       aes(x = food_regime)) +
   geom_point(aes(y = .pred)) +
   geom_errorbar(aes(ymin = .pred_lower,
                     ymax = .pred_upper),
                 width = .2) +
   labs(y = "urchin size")
```

![](rstats_anova_lm_files/figure-gfm/unnamed-chunk-24-1.png)<!-- -->

There is also an example of a *Bayesian* model in the tidymodels article
I have not included here.

## Close project

Closing RStudio will ask you if you want to save your workspace and
scripts. Saving your workspace is usually not recommended if you have
all the necessary commands in your script.

## Useful links

-   For statistical analysis in R:

    -   Steve Midway’s [Data Analysis in R Part II
        Analysis](https://bookdown.org/steve_midway/DAR/part-ii-analysis.html)
    -   Jeffrey A. Walker’s [Applied Statistics for Experiemental
        Biology](https://www.middleprofessor.com/files/applied-biostatistics_bookdown/_book/)
    -   Chester Ismay and Albert Y. Kim’s [ModernDive Statistical
        Inference via Data Science](https://moderndive.com/)

-   For tidymodels:

    -   [tidymodels website](https://www.tidymodels.org/)

-   Our compilation of [general R
    resources](https://gitlab.com/stragu/DSH/blob/master/R/usefullinks.md)
